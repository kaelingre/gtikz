# gTikZ: Drawing Feynman graphs in Latex using TikZ

## Usage

* Copy the file `gTikZ.tex` or `gTikZBeamer.tex` into your project folder.
* Include in latex source via `\input{gTikZ}` or `\input{gTikzBeamer}`
* For more information build the `graphTest.tex` file using `pdflatex --shell-escape graphTest.tex` and read the manual in the output PDF file