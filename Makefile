LATEX=pdflatex
LATEXOPT=--shell-escape
NONSTOP=--interaction=nonstopmode

LATEXMK=latexmk
LATEXMKOPT=-pdf

all: graphTest.pdf graphTestBeamer.pdf

graphTest.pdf: graphTest.tex gTikZ.tex
	$(LATEXMK) $(LATEXMKOPT) -pdflatex="$(LATEX) $(LATEXOPT) $(NONSTOP) %O %S" graphTest

graphTestBeamer.pdf: graphTestBeamer.tex gTikZBeamer.tex
	$(LATEXMK) $(LATEXMKOPT) -pdflatex="$(LATEX) $(LATEXOPT) $(NONSTOP) %O %S" graphTestBeamer

clean:
	rm -f *~ *.log *.aux *.pdf figures/* *.auxlock *.fls

.PHONY: all
